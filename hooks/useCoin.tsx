import useSWR from 'swr';
import { API_URL } from 'constants/API';
import { fetcher } from 'utils';

export const useCoin = (id: any) => {
  const { data, error } = useSWR(`${API_URL}/${id}`, fetcher);

  return {
    coin: data,
    isLoading: !error && !data,
    isError: error,
  };
};
