import useSWR from 'swr';
import { fetcher } from 'utils';
import { API_URL } from 'constants/API';

export const useMarkets = (currency: string) => {
  const { data, error } = useSWR(
    `${API_URL}/markets?vs_currency=${currency}&per_page=10`,
    fetcher
  );

  return {
    markets: data,
    isLoading: !error && !data,
    isError: error,
  };
};
