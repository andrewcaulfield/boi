import * as React from 'react';
import List from '@mui/material/List';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import Avatar from '@mui/material/Avatar';
import Typography from '@mui/material/Typography';
import { Market } from '../../types/Market';
import Link from 'next/link';

interface MarketListItemProps {
  market: Market;
}

const MarketListItem = ({
  market: { image, id, symbol, current_price, high_24h, low_24h },
}: MarketListItemProps) => {
  return (
    <List sx={{ width: '100%', maxWidth: 900, bgcolor: 'background.paper' }}>
      <Link href={`coins/${encodeURIComponent(id)}`}>
        <ListItemButton alignItems="flex-start">
          <ListItemAvatar>
            <Avatar alt={symbol} src={image} />
          </ListItemAvatar>
          <ListItemText
            primary={
              <>
                <Typography>
                  {id} ({symbol.toUpperCase()})
                </Typography>
                <Typography>current price: €{current_price}</Typography>
              </>
            }
            primaryTypographyProps={{
              textTransform: 'capitalize',
              display: 'flex',
              justifyContent: 'space-evenly',
            }}
            secondaryTypographyProps={{
              display: 'flex',
              justifyContent: 'space-evenly',
            }}
            secondary={
              <>
                <Typography>24 hour high price: €{high_24h}</Typography>
                <Typography>24 hour low price: €{low_24h}</Typography>
              </>
            }
          />
        </ListItemButton>
      </Link>
    </List>
  );
};

export default MarketListItem;
