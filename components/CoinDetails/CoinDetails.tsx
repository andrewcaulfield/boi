import styles from 'styles/Coin.module.css';
import sanitizeHtml from 'sanitize-html';
import Button from '@mui/material/Button';
import { useCoin } from 'hooks/useCoin';
import Skeleton from '@mui/material/Skeleton';

interface CoinProps {
  id: string | string[] | undefined;
}

const CoinDetails = ({ id }: CoinProps) => {
  const { coin, isLoading, isError } = useCoin(id);

  const coinListing = !isLoading ? (
    <>
      <div className={styles.coin}>
        <h3 className={styles.coin_label}>Symbol: </h3>
        <span className={styles.coin_description}>
          {coin?.symbol.toUpperCase()}
        </span>
      </div>
      <div className={styles.coin}>
        <h3 className={styles.coin_label}>Description: </h3>
        <span
          className={styles.coin_description}
          dangerouslySetInnerHTML={{
            __html: sanitizeHtml(
              coin && coin?.description?.en
                ? coin?.description?.en
                : 'Description not available',
              {
                allowedTags: ['a'],
                allowedAttributes: {
                  a: ['href'],
                },
              }
            ),
          }}
        ></span>
      </div>
      <div className={styles.coin}>
        <h3 className={styles.coin_label}>Hashing Algorithm: </h3>
        <span className={styles.coin_description}>
          {coin?.hashing_algorithm ?? 'Hashing algorithm not available'}
        </span>
      </div>
      <div className={styles.coin}>
        <h3 className={styles.coin_label}>Market Cap Rank: </h3>
        <span className={styles.coin_description}>
          {coin?.market_cap_rank ?? 'Market Cap Rank not available'}
        </span>
      </div>
      <div className={styles.coin}>
        <h3 className={styles.coin_label}>Homepage: </h3>
        <span className={styles.coin_description}>
          <a href={coin?.links?.homepage[0]}>
            {coin?.links?.homepage[0] ?? 'Homepage not available'}
          </a>
        </span>
      </div>
      <div className={styles.coin}>
        <h3 className={styles.coin_label}>Genesis Date: </h3>
        <span className={styles.coin_description}>
          {coin?.genesis_date ?? 'Genesis date not available'}
        </span>
      </div>
      <Button
        href="/"
        variant="outlined"
        style={{ justifyContent: 'flex-start' }}
      >
        Back
      </Button>
    </>
  ) : (
    <Skeleton variant="rectangular" width={210} height={118} />
  );

  return <>{isError ? <h2>Something went wrong...</h2> : coinListing}</>;
};

export default CoinDetails;
