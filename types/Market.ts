export interface Market {
  current_price: number;
  high_24h: number;
  id: string;
  image: string;
  low_24h: number;
  symbol: string;
}
