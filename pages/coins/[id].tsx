import type { NextPage } from 'next';
import layout from 'styles/Main.module.css';
import Head from 'next/head';
import CoinDetails from 'components/CoinDetails';
import { useRouter } from 'next/router';

const Coins: NextPage = () => {
  const router = useRouter();

  const { id } = router.query;
  return (
    <>
      <Head>
        <title>Coin : {id}</title>
      </Head>
      <div className={layout.container}>
        <h1 className={layout.title}>{id}</h1>
        <main className={layout.main}>
          <div className={layout.grid}>{<CoinDetails id={id} />}</div>
        </main>
      </div>
    </>
  );
};

export default Coins;
